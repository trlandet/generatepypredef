Automatically generate ``*.pypredef`` files
===========================================

Generate ``*.pypredef`` files for Eclipse PyDev.

The generated files may also work with PyCharm(?) and other editors.


Purpose
-------

This tool is useful for developing with auto-completion in an IDE when the developed code is meant to be running in a container / environment which is hard to replicate in the IDE. I use this for generating autocompletion files in Singularity or Docker for FEniCS_' ``dolfin`` package which is a C++ / pybind11 code that can be tedious to install with the system Python, but is trivial to run in a container.

It is most useful to use this tool to enable auto-completion for compiled dependencies. Using it for the code being develped will also work, but then you will need to re-run the generation every time you change the API instead of every time you upgrade the dependency.

.. _FEniCS: http://fenicsproject.org/

Tested with ``dolfin``, ``h5py``, ``mpi4py``, ``petsc4py``, ``numpy`` and ``scipy``. 

Usage
-----

Run the program with the top level package names as arguments on the command line::

  python3 autogen_pypredef.py package_1 package_2 ...
  
All sub-packages should be identified automatically. The generated ``*.pypredef`` files are written to the current directory. The program may print some warnings about unsupported types (some compiled C extension types are not well supported), but autocompletion will in most cases work anyway.

To verify that the generated code does not contain bugs due to errors in the generator, run with::

  python3 autogen_pypredef.py package_1 package_2 ... --test


License
-------

Copyright Tormod Landet, 2017. Licensed under the Apache 2.0 license, a permissive free software license compatible with version 3 of the GNU GPL.