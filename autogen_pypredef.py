"""
Automatically generate ``*.pypredef`` files
===========================================

Generate ``*.pypredef`` files for Eclipse PyDev

The generated files may also work with PyCharm(?) and other editors.

Run the program with the top level package names as arguments on the command line::

  python3 autogen_pypredef.py package_1 package_2 ...
  
All sub-packages should be identified automatically. The generated ``*.pypredef`` files
are written to the current directory.

To verify that the generated code does not contain bugs due to errors in the generator,
run with::

  python3 autogen_pypredef.py package_1 package_2 ... --test
  
(c) Tormod Landet, 2017, Apache 2.0 license
"""
import sys, os
import importlib, inspect
import textwrap
import collections
import traceback
import math


BLACKLIST = {'__init__', '__pycache__', '__main__',
             'test', 'tests', 'testing', 'distutils'}
CONSTANTS = (str, int, float, bool, list, tuple, dict, set, property)
SEEN = set()


class DefConstant:
    def __init__(self, name, type_code):
        self.name = name
        self.type_code = type_code
    
    def gen(self, indent=0):
        indent1 = ' ' * indent
        return '%s%s = %s\n' % (indent1, self.name, self.type_code)
    

def format_doc(docstring, indent):
    if docstring:
        docstring = docstring.replace('\\', '\\\\')
        docstring = docstring.replace('"""', '\\"""')
        d = textwrap.indent(docstring, indent)
        return '%s"""\n%s\n%s"""' % (indent, d, indent)
    else:
        return '%s""' % indent


def format_args(args):
    if not args:
        return '(*argv, **kwargs)'
    
    EMPTY = inspect.Parameter.empty
    params = collections.OrderedDict(args.parameters)
    items = list(params.items())
    
    for pname, param in items:
        if param.default is EMPTY:
            continue
        elif isinstance(param.default, (int, str)):
            continue
        elif isinstance(param.default, float):
            if math.isinf(param.default) or math.isnan(param.default):
                replacement = 'float(%r)' % param.default
            else:
                continue
        elif param.default is None:
            continue
        elif param.default == ():
            continue
        else:
            replacement = repr(param.default)
        params[pname] = param.replace(default=replacement)
    
    return str(args.replace(parameters=params.values()))


class DefFunc:
    def __init__(self, name, args, docstring):
        self.name = name
        self.args = args
        self.docstring = docstring
    
    def gen(self, indent=0):
        indent1 = ' ' * (indent)
        indent2 = ' ' * (indent + 4)
        args = format_args(self.args)
        doc = format_doc(self.docstring, indent2)
        return '\n%sdef %s%s:\n%s\n%spass\n\n' % (indent1, self.name, args, doc, indent2)


class DefClass:
    def __init__(self, name, docstring):
        self.name = name
        self.docstring = docstring
        self.members = []
    
    def gen(self, indent=0):
        indent1 = ' ' * (indent)
        indent2 = ' ' * (indent + 4)
        doc = format_doc(self.docstring, indent2)
        code = ['\n%sclass %s:\n%s\n%spass\n\n' % (indent1, self.name, doc, indent2)]
        for m in self.members:
            code += m.gen(indent + 4)
        
        return ''.join(code)


def process_const(defs, name, obj):
    if isinstance(obj, str):
        defs.append(DefConstant(name, 'str'))
    elif isinstance(obj, int):
        defs.append(DefConstant(name, 'int'))
    elif isinstance(obj, float):
        defs.append(DefConstant(name, 'float'))
    elif isinstance(obj, bool):
        defs.append(DefConstant(name, 'bool'))
    elif isinstance(obj, list):
        defs.append(DefConstant(name, 'list'))
    elif isinstance(obj, tuple):
        defs.append(DefConstant(name, 'tuple'))
    elif isinstance(obj, dict):
        defs.append(DefConstant(name, 'dict'))
    elif isinstance(obj, set):
        defs.append(DefConstant(name, 'set'))
    else:
        defs.append(DefConstant(name, 'None'))


def process_function(defs, name, obj):
    args = inspect.signature(obj)
    doc = inspect.getdoc(obj)
    defs.append(DefFunc(name, args, doc))
    

def process_function2(defs, name, obj):
    """
    The inspect.getargspec() function does not work for all instance methods
    Failes for C/C++ etc compiled code? Try, e.g., with the builtins like id()
    """
    args = []
    doc = ''
    if hasattr(obj, '__doc__') and isinstance(obj.__doc__, str):
        doc = textwrap.dedent(obj.__doc__)
    defs.append(DefFunc(name, args, doc))


def process_class(defs, name, obj):
    doc = inspect.getdoc(obj)
    c = DefClass(name, doc)
    defs.append(c)
    return process_namespace(c.members, obj)


def process_namespace(defs, ns):
    # Avoid infinite loops
    if id(ns) in SEEN:
        return 0
    SEEN.add(id(ns))
    
    # Loop over namespace members
    num_warnings = 0
    for name in dir(ns):
        if name in BLACKLIST or name.startswith('__'):
            continue
        
        obj = getattr(ns, name)
        
        if (isinstance(obj, CONSTANTS) or inspect.ismemberdescriptor(obj) or
            inspect.isgetsetdescriptor(obj) or obj is None):
            process_const(defs, name, obj)
        
        elif inspect.ismodule(obj):
            continue
        
        elif inspect.isclass(obj):
            num_warnings += process_class(defs, name, obj)
        
        elif inspect.isfunction(obj) or inspect.ismethod(obj):
            # Callable where we can get the argspec
            process_function(defs, name, obj)
        
        elif inspect.isbuiltin(obj) or inspect.ismethoddescriptor(obj):
            # Callable where we cannot get the argspec
            process_function2(defs, name, obj)
        
        else:
            # Not implemented yet
            print('WARNING: %s has unsupported type %r, assuming it is None' % (name, type(obj)))
            num_warnings += 1
            process_const(defs, name, obj)
    
    return num_warnings


def process_module(mod, filename):
    """
    Generate pypredef for one module and save results to the given file name
    """
    defs = []
    nwarn = process_namespace(defs, mod)
    
    print('Writing %r' % filename)
    with open(filename, 'wt') as out:
        for d in defs:
            code = d.gen()
            out.write(code)
    
    return nwarn


def process_package(name):
    """
    Generate pypredef for all modules under the given package
    Returns the names of the generated files
    """
    for pth in sys.path:
        fullpath = os.path.join(pth, name)
        basepath = pth
        if os.path.exists(fullpath):
            break
    else:
        print('ERROR: package not found in sys.path:', name)
        return [], 0, 1
    
    print('\n' + '-' * 80)
    print('Processing package', name, 'found in', fullpath)
    modules = treesearch(basepath, name)
    filenames = []
    nwarn = 0
    for mod_name in modules:
        try:
            print('Importing %r' % mod_name)
            mod = importlib.import_module(mod_name)
        except Exception:
            continue
        filename = mod_name + '.pypredef'
        nwarn += process_module(mod, filename)
        filenames.append(filename)
    
    return filenames, nwarn, 0


def treesearch(basepath, name):
    """
    Recursively find importable packages below ``basepath/name``
    May return false positives!
    """
    yield name
    
    dirname = os.path.join(basepath, name.split('.')[-1])
    if os.path.isdir(dirname):
        for child in os.listdir(dirname):
            if '.' in child:
                i = child.find('.')
                child = child[:i]
            if child in BLACKLIST:
                print('Skipping %r (in BLACKLIST)' % child)
            else:
                yield from treesearch(dirname, '%s.%s' % (name, child))


def test_code_syntax(code):
    """
    Running the generated code should not do anything except
    checking the syntax since the generated files just contain
    definitions
    """
    try:
        exec(code)
        return 0
    except Exception:
        print('ERROR: generated code has a syntax error')
        traceback.print_exc(file=sys.stdout)
        return 1


if __name__ == '__main__':
    packages = sys.argv[1:]
    
    test = False
    if '--test' in packages:
        packages.remove('--test')
        test = True
    
    print('Running autogen_pypredef on the following packages:', packages)
    filenames = []
    num_warnings = num_errors = 0
    for name in packages:
        fns, nwarn, nerr = process_package(name)
        filenames.extend(fns)
        num_warnings += nwarn
        num_errors += nerr
    
    if test:
        print('\n\n' + '=' * 80)
        print('Syntax testing generated files')
        for fn in filenames:
            print('Testing %r' % fn)
            with open(fn, 'rt') as inp:
                code = inp.read()
            
            num_errors += test_code_syntax(code)
    
    if num_warnings:
        print('\nThere were %d warnings about unsupported types ' % num_warnings +
              '(probably C extension types)')
        if not num_errors:
            print('Autocompletion will most likely still work\n')
    
    if num_errors:
        print('\nThere were %d errors' % num_errors)
        print('Autocompletion will not work as intended!\n')
        
    exit(num_errors)
